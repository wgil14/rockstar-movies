# 🤘 Rockstar Movies 🤘

[Live demo](https://rockstar-movies.netlify.app/)
![Hero image](https://i.ibb.co/31Dxn4B/Screen-Shot-2021-01-26-at-2-18-40-AM.png)

Discover movies code challenge.
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Project installation

1. Clone this repository using:

```
git@gitlab.com:wgil14/rockstar-movies.git
```

2. Install packages by using:

```
npm install
```

3. Either Copy the `.env.example` file and paste it as `.env` or fill it with your own keys.

4. Run the project:

```
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
