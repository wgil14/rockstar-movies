import { RouteComponentProps } from '@reach/router'
import React, { useMemo, useState } from 'react'
import Helmet from 'react-helmet'
import { GetError } from './GetError'
import { MoviePreview } from './MoviePreview'
import { StarsFilter } from './StarsFilter'
import { MovieDiscover, useDiscoverMovies, useMovieSearch } from './utils/Movies'

export const Discover = (props: RouteComponentProps) => {
  const [query, setQuery] = useState('')
  const movies = useDiscoverMovies()
  const movieSearch = useMovieSearch()
  const [averageFilter, setAverageFilter] = useState({ min: 0, max: 10 })
  const moviesStream = useMemo(() => {
    const filterByAverage = (movie: MovieDiscover) =>
      movie.vote_average >= averageFilter.min && movie.vote_average <= averageFilter.max

    if (query && movieSearch.data?.results?.length) {
      return movieSearch?.data?.results?.filter(filterByAverage)
    }

    return movies.data?.results?.filter(filterByAverage)
  }, [averageFilter.max, averageFilter.min, query, movieSearch.data, movies.data?.results])

  const search = () => {
    const url = new URL(movieSearch.request.toString())
    const searchParams = new URLSearchParams(url.search)
    searchParams.set('query', query)
    url.search = searchParams.toString()
    movieSearch.get(new Request(url.toString()))
  }

  if (movies.loading || movieSearch.loading) {
    return <p>Loading 🤞🏼...</p>
  }

  if (movies.error) {
    return <GetError />
  }

  return (
    <>
      <Helmet>
        <title>Rockstar Movies</title>
      </Helmet>
      <section>
        <div className="filters">
          <form>
            <label htmlFor="search-box">Search: </label>
            <input
              id="search-box"
              type="text"
              placeholder="Type and press ⎆Enter to discover movies!"
              value={query}
              onChange={(e) => {
                if (!e.target.value) movieSearch.reset()
                setQuery(e.target.value)
              }}
              onKeyPress={(e) => {
                if (e.key !== 'Enter' || !e.currentTarget.value) return

                search()
              }}
              autoFocus
            />
            <button
              className="primary"
              type="submit"
              disabled={!query}
              onClick={(e) => {
                e.preventDefault()
                search()
              }}
            >
              Search
            </button>
          </form>
          <StarsFilter onChange={setAverageFilter} />
        </div>
        <div className="discover">
          <h3>Discover</h3>
          {!!query && !!movieSearch.data?.results && !!moviesStream?.length && (
            <i>{movieSearch.data?.results.length} movies found.</i>
          )}
          <ul>
            {moviesStream?.length ? (
              moviesStream?.map((movie) => <MoviePreview movie={movie} key={movie.id} />)
            ) : (
              <li>
                <i>There are no movies with that score.</i>
              </li>
            )}
          </ul>
        </div>
      </section>
    </>
  )
}
