import React, { useState } from 'react'
import { Star } from './Star'

const initialState = [false, false, false, false, false]
type Average = { min: number; max: number }
const averageMap: Record<number, Average> = {
  0: { min: 0, max: 2 },
  1: { min: 2, max: 4 },
  2: { min: 4, max: 6 },
  3: { min: 6, max: 8 },
  4: { min: 8, max: 10 },
}

type Props = {
  onChange: (average: Average) => void
}

export const StarsFilter = ({ onChange }: Props) => {
  const [activeStars, setActiveStars] = useState<boolean[]>(initialState)

  const handleClick = (idx: number) => {
    setActiveStars((activeStars) => {
      const isLastActiveStar = activeStars[idx] && !activeStars[idx + 1]
      if (isLastActiveStar) {
        onChange({ min: 0, max: 10 })
        return initialState
      }

      onChange(averageMap[idx])
      const isSmallerStar = activeStars[idx + 1]
      if (isSmallerStar) return [...activeStars.slice(0, idx + 1), ...activeStars.slice(idx + 1).map(() => false)]

      return [...activeStars.slice(0, idx + 1).map(() => !activeStars[idx]), ...activeStars.slice(idx + 1)]
    })
  }

  return (
    <div className="stars">
      <p>Filter by popularity </p>
      <Star idx={0} active={activeStars[0]} onClick={handleClick} />{' '}
      <Star idx={1} active={activeStars[1]} onClick={handleClick} />{' '}
      <Star idx={2} active={activeStars[2]} onClick={handleClick} />{' '}
      <Star idx={3} active={activeStars[3]} onClick={handleClick} />{' '}
      <Star idx={4} active={activeStars[4]} onClick={handleClick} />{' '}
    </div>
  )
}
