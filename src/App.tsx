import React from 'react'
import { Redirect, Router } from '@reach/router'
import { Discover } from './Discover'
import { Movie } from './Movie'
import { Logo } from './Logo'
import { NotFound } from './NotFound'

export const App = () => {
  return (
    <>
      <header>
        <nav>
          <Logo />
        </nav>
      </header>
      <div className="hero">
        <h1>Rockstar Movies</h1>
      </div>
      <main>
        <Router>
          <Redirect from="/" to="/discover" noThrow />
          <Discover path="/discover" />
          <Movie path="/movies/:movieId" />
          <NotFound default />
        </Router>
      </main>
      <footer>
        <p>
          All data shown belongs to{' '}
          <a href="https://themoviedb.org" target="__BLANK">
            TMDb
          </a>{' '}
          and there is no commercial usage intended.
        </p>
        <img
          width="50"
          alt={`TMDb`}
          src="https://www.themoviedb.org/assets/2/v4/logos/v2/blue_square_1-5bdc75aaebeb75dc7ae79426ddd9be3b2be1e342510f8202baf6bffa71d7f5c4.svg"
        />
      </footer>
    </>
  )
}
