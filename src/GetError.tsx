import React from 'react'

export const GetError = () => {
  return <p>Whoops! It seems there was an error, try reloading the page.</p>
}
