import { Link } from '@reach/router'
import React from 'react'
import { useMoviesContext } from './context/MoviesContext'
import { MovieDiscover } from './utils/Movies'

type Props = { movie: MovieDiscover; preview?: boolean }

export const MoviePreview = ({ movie, preview = true }: Props) => {
  const { imgUrl } = useMoviesContext()

  return (
    <li className="movie">
      <h4>
        <Link to={`/movies/${movie.id}`}>{movie.original_title}</Link>
      </h4>
      <p className="overview">{movie.overview}</p>
      {preview && <Link to={`/movies/${movie.id}`}>See more.</Link>}
      <img src={`${imgUrl}/w500${movie.poster_path}`} alt={`${movie.original_title} poster`} />
      <p>Original language: {movie.original_language}</p>
      <p>
        Votes average: {movie.vote_average} out of {movie.vote_count}
      </p>
      <p>Public: {movie.adult ? 'adults' : 'everyone'}</p>
      <p>Released on: {new Date(movie.release_date).toLocaleString()}</p>
    </li>
  )
}
