import { RouteComponentProps } from '@reach/router'
import React from 'react'
import Helmet from 'react-helmet'

export const NotFound = (props: RouteComponentProps) => (
  <>
    <Helmet>
      <title>Page not found</title>
    </Helmet>
    <h3>Nothing here 😭</h3>
  </>
)
