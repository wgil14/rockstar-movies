import React from 'react'

type Props = {
  active: boolean
  onClick: (id: number) => void
  idx: number
}

export const Star = ({ onClick, active = false, idx }: Props) => (
  <button className="star" aria-pressed={active} onClick={() => onClick(idx)}>
    <span style={{ opacity: active ? 1 : 0.3 }} aria-label={`${idx + 1} stars.`}>
      ⭐
    </span>
  </button>
)
