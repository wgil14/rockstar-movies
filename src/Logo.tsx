import React from 'react'
import { Link } from '@reach/router'

export const Logo = () => <Link to="/">🤘Home</Link>
