import { useMoviesContext } from '../context/MoviesContext'
import { useApi } from './useApi'

export type Movie = {
  id: number
  original_title: string
  backdrop_path: string
  budget: number
  imdb_id: string
  original_language: string
  overview: string
  popularity: number
  poster_path: string
  production_companies: { id: string; logo_path: string; name: string; origin_country: string }[]
  production_countries: { iso_3166_1: string; name: string }[]
  revenue: number
  runtime: number
  spoken_languages: { english_name: string; iso_639_1: string; name: string }[]
  release_date: string
  status: string
  tagline: string
  video: boolean
  vote_average: number
  vote_count: number
  adult: boolean
  gnres: { id: number; name: string }[]
  homepage: string
}

/**
 * Get a movie by id.
 * @param movieId string
 */
export const useMovie = (movieId: string) => {
  const { apiKey, apiUrl } = useMoviesContext()

  return useApi<Movie>(`${apiUrl}/movie/${movieId}?api_key=${apiKey}`)
}

export type MovieDiscover = {
  adult: boolean
  backdrop_path: string
  id: number
  original_language: string
  original_title: string
  overview: string
  popularity: number
  poster_path: string
  release_date: string
  video: boolean
  vote_average: number
  vote_count: number
}

/**
 * Discover movies.
 *
 * @param movieId string
 */
export const useDiscoverMovies = () => {
  const { apiKey, apiUrl } = useMoviesContext()

  return useApi<{ results: MovieDiscover[]; page: number; total_pages: number; total_results: number }>(
    `${apiUrl}/discover/movie?api_key=${apiKey}`
  )
}

/**
 * Show movie results based on a given query string.
 *
 * @param query string
 */
export const useMovieSearch = () => {
  const { apiKey, apiUrl } = useMoviesContext()

  return useApi<{ results: MovieDiscover[]; page: number; total_pages: number; total_results: number }>(
    `${apiUrl}/search/movie?api_key=${apiKey}`,
    false
  )
}
