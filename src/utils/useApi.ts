import { useCallback, useEffect, useReducer, useState } from 'react'

export type Response<T> =
  | { data: null; loading: true; error: false }
  | { data: null; loading: false; error: true }
  | { data: null; loading: false; error: false }
  | { data: T; loading: false; error: false }

type Action<T> =
  | { type: 'REQUEST' }
  | { type: 'SUCCESS'; data: T }
  | { type: 'ERROR'; error: string }
  | { type: 'RESET' }

const createApiReducer = <T>() => (previousState: Response<T>, action: Action<T>): Response<T> => {
  switch (action.type) {
    case 'REQUEST': {
      return { loading: true, data: null, error: false }
    }
    case 'SUCCESS': {
      return { loading: false, data: action.data, error: false }
    }
    case 'ERROR': {
      return { loading: false, data: null, error: true }
    }
    case 'RESET': {
      return { loading: false, error: false, data: null }
    }
    default:
      return previousState
  }
}

type UseApi<T> = {
  request: RequestInfo
  get: (request: RequestInfo) => Promise<void>
  reset: () => void
} & Response<T>

/**
 * Manage state of remote resources.
 *
 * @param request RequestInfo
 * @return Response<T>
 */
export const useApi = <T>(request: RequestInfo, auto = true): UseApi<T> => {
  const [{ loading, data, error }, dispatch] = useReducer(createApiReducer<T>(), {
    loading: auto,
    data: null!,
    error: false,
  })
  const [isMounted, setIsMounted] = useState(true)

  const get = useCallback(
    async (req = request) => {
      try {
        if (!auto) {
          dispatch({ type: 'REQUEST' })
        }

        const response = await fetch(req)
        if (!response.ok) {
          if (!isMounted) return
          dispatch({ type: 'ERROR', error: await response.json() })
          return
        }

        if (!isMounted) return
        dispatch({ type: 'SUCCESS', data: await response.json() })
      } catch (e) {
        if (!isMounted) return
        dispatch({ type: 'ERROR', error: e })
      }
    },
    [isMounted, request, auto]
  )

  const reset = useCallback(() => {
    dispatch({ type: 'RESET' })
  }, [])

  useEffect(() => {
    if (auto) {
      get()
    }
  }, [auto, get])

  useEffect(() => {
    return () => {
      setIsMounted(false)
    }
  }, [])

  if (loading) {
    return { data: null, loading, error: false, get, request, reset }
  } else if (error || !data) {
    return { data: null, loading, error: true, get, request, reset }
  }

  return { data, loading, error: false, get, request, reset }
}
