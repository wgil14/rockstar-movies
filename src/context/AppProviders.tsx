import React, { ReactNode } from 'react'
import { MoviesContextProvider } from './MoviesContext'

type Props = {
  children: ReactNode
}

/**
 * All application providers should live here.
 */
export const AppProviders = ({ children }: Props) => {
  return <MoviesContextProvider>{children}</MoviesContextProvider>
}
