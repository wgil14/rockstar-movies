import React, { createContext, ReactNode, useContext } from 'react'

const Context = createContext<{ apiKey: string; apiUrl: string; imgUrl: string } | undefined>(undefined)

type Props = {
  children: ReactNode
}

const apiUrl = process.env.REACT_APP_THE_MOVIE_DB_API_URL
const apiKey = process.env.REACT_APP_THE_MOVIE_DB_API_KEY
const imgUrl = process.env.REACT_APP_THE_MOVIE_DB_IMG_URL

/**
 * Context provider to provide environment variables to the application services.
 *
 */
export const MoviesContextProvider = ({ children }: Props) => {
  if (!apiKey) {
    throw new Error(
      'Movies API key is not properly set. Please refer to https://gitlab.com/wgil14/rockstar-movies for setup instructions.'
    )
  }

  if (!apiUrl) {
    throw new Error(
      'Movies API URL is not properly set. Please refer to https://gitlab.com/wgil14/rockstar-movies for setup instructions.'
    )
  }

  if (!imgUrl) {
    throw new Error(
      'Movies IMG URL is not properly set. Please refer to https://gitlab.com/wgil14/rockstar-movies and https://developers.themoviedb.org/3/getting-started/images for setup instructions.'
    )
  }

  return <Context.Provider value={{ apiKey, apiUrl, imgUrl }}>{children}</Context.Provider>
}

/**
 * Get the Movies environment variables.
 *
 */
export const useMoviesContext = () => {
  const context = useContext(Context)

  if (!context) {
    throw new Error('`useMoviesContext` must be used within MoviesContextProvider.')
  }

  return context
}
