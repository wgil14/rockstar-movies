import React from 'react'
import { navigate, RouteComponentProps } from '@reach/router'
import { useMovie } from './utils/Movies'
import { GetError } from './GetError'
import { MoviePreview } from './MoviePreview'
import { Helmet } from 'react-helmet'

export const Movie = ({ movieId }: RouteComponentProps<{ movieId: string }>) => {
  const movie = useMovie(movieId!)

  if (movie.loading) return <p>Loading movie...</p>
  if (movie.error) return <GetError />

  return (
    <>
      <Helmet>
        <title>{movie.data?.original_title}</title>
      </Helmet>
      <section>
        <button className="primary" onClick={() => navigate('/discover')}>{`👈 Discover`}</button>
        {movie.data && <MoviePreview movie={movie.data} preview={false} />}
      </section>
    </>
  )
}
